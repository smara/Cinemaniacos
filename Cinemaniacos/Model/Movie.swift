//
//  Movie.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 23/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation


struct Movie {
    

    let id: String?
    let ancineID: String?
    let duration: String?
    var rate: Int?
    let contentRating: String?
    let genre: [String]?
    let title: String?
    let premier: PremiereDate?
    let synopsis: String?
    
    let casting: [Casting]?

    let director: String?
    let priority: Int?
    let urlKey: String?  // City related ?
    let siteUrl: String?
    let tags: [String]?
    let countIsPlaying: Int?
    
    let trailers: [Trailer]?
    let posters: [Poster]?
    
    

    
}


// Helpers
struct Trailer {
    let type: String?
    let url: String?
    let embedURL: String?
}

struct Poster {
    let url: String?
    let type: String?
}

struct PremiereDate {
    let localDate: String?
    let isToday: Bool?
    let dayOfWeek: String?
    let dayAndMonth: String?
    let hour: String?
    let year: String?
}















