//
//  EmCartazViewController.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 22/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class EmCartazViewController: UIViewController, UICollectionViewDataSource {
  
    weak var trailerPageVC: TrailerPageVC!
    // pass the videos url to the trailer page view controller

    @IBOutlet weak var emCartazCollection: UICollectionView!
    
    let emCartazCellId = "emCartazCell"
    
    var testModel = [Movie]()
    var movieFull: Movie!
    
    var emCartazMovies: [Movie]?

    
    override func viewDidLoad() {
        super.viewDidLoad()
        // obter o modelo do webservice ou cache
        //fetchMovies
        // all playing
        let endPoint = Methods.nowPlaying   // that's the resource I need for this Controller for both APIs
    }

    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showPageVC" {
            trailerPageVC = segue.destination as! TrailerPageVC
            trailerPageVC.model = ["https://www.youtube.com/embed/PmUL6wMpMWw", "https://www.youtube.com/embed/WCJOsNjBFF0", "https://www.youtube.com/embed/MJ3eEI9o4nA"]
        } else if segue.identifier == "showFilmeVC" {
            let filmeVC = segue.destination as! FilmeViewController
            filmeVC.model = movieFull
        }
    }
   
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return testModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emCartazCellId, for: indexPath) as! EmCartazCell
        
        let movie = testModel[indexPath.row]
        
        cell.movieName.text = movie.title
//        cell.posterImage.image = UIImage(named: movie.posterTest)
        
        return cell
    }
    
   
    

}




class Mock: EmCartazViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

//
//        let movie1 = Movie(name: "Encantados", posterTest: "Encantados")
//        let movie2 = Movie(name: "Red Sparrow", posterTest: "RedSparrow")
//        let movie3 = Movie(name: "O Passageiro", posterTest: "OPassageiro")
//        let movie4 = Movie(name: "Os Farofeiros", posterTest: "OsFarofeirosImgLarge")
//
//
//        testModel.append(movie1)
//        testModel.append(movie2)
//        testModel.append(movie3)
//        testModel.append(movie4)
//
//
//         let youtubeURL = "https://www.youtube.com/embed/PmUL6wMpMWw?playsinline=1&rel=0&showinfo=0"
//        movieFull = testModel[2]
//        movieFull.trailerURL = youtubeURL
//
//        movieFull.duration = "140 min"
//        movieFull.genre = "Comédia"
//        movieFull.contentRating = "18"
//        movieFull.title = "O Passageiro"
//        movieFull.director = "José da Silva"
//        movieFull.sound = "João Manuel"
//        movieFull.synopsis = "Outrora talentosa bailarina, Dominika Egorova é convencida a se tornar uma Sparrow, ou seja, uma sedutora treinada na melhor escola de espionagem russa. Após passar pelo árduo processo de aprendizagem, ela se torna a mais talentosa espiã do país"
//
//        movieFull.casting =  [Casting(name: "Jennifer Lawrence", job: "Atriz", character: "Dominika Egorova")]
//        movieFull.casting?.append(Casting(name: "Joel Edgerton", job: "Ator", character: "Nate Nash"))
//
//        movieFull.otherMediaURLs = ["https://www.youtube.com/embed/PmUL6wMpMWw", "https://www.youtube.com/embed/WCJOsNjBFF0", "https://www.youtube.com/embed/MJ3eEI9o4nA"]
    }

    
}















