//
//  FavoritosViewController.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 29/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit




class FavoritosViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UITableViewDataSource, UITableViewDelegate {
   
    
    @IBOutlet weak var favoritosView: UIView!
    @IBOutlet weak var watchListView: UIView!
    
    @IBOutlet weak var favoritosButton: UIButton! {
        didSet {
            favoritosButton.setImage(UIImage(named: "Selector Favoritos Deselected"), for: .normal)
            favoritosButton.setImage(UIImage(named: "Selector Favoritos Selected"), for: .disabled)
            favoritosButton.isEnabled = false
        }
    }
    
    @IBOutlet weak var watchButton: UIButton! {
        didSet {
            watchButton.setImage(UIImage(named: "Selector Watch Deselected"), for: .normal)
            watchButton.setImage(UIImage(named: "Selector Watch Selected"), for: .disabled)
        }
    }
    
    @IBOutlet weak var favoritosLeadingConstraint: NSLayoutConstraint!
    
    
    let favoritoCellID = "favoritoCell"
    let watchListCellID = "watchListCell"
    let segueID = "showMoviesFromFavorites"
    
    var model: [Movie]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
 

        self.view.addConstraint(NSLayoutConstraint(item: favoritosView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 1, constant: 0))
        self.view.addConstraint(NSLayoutConstraint(item: watchListView, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 1, constant: 0))
        
    }

    
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        favoritosLeadingConstraint.constant = favoritosButton.isEnabled ? -size.width : 0
    }
    
    @IBAction func switchViewsButton(_ sender: UIButton) {
        if sender == favoritosButton {
            favoritosLeadingConstraint.constant = 0
        } else {
            favoritosLeadingConstraint.constant = -self.view.frame.width
        }
        
        UIView.animate(withDuration: 0.3, delay: 0, options: .beginFromCurrentState, animations: {
            self.view.layoutIfNeeded()
        })
        
        favoritosButton.isEnabled = !favoritosButton.isEnabled
        watchButton.isEnabled = !watchButton.isEnabled
    }
    
   
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueID {
            if let destination = segue.destination as? FilmeViewController {
                if let index = (sender as? IndexPath)?.item, let model = self.model, index > model.count {
                    destination.model = model[index]
                }
            }
        }
    }
    
    
    
    
    // MARK: - UICollectionViewDataSource
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: favoritoCellID, for: indexPath) as? EmCartazCell {
        
            cell.posterImage.image = UIImage(named: "RedSparrow")
            cell.movieName.text = "Red Sparrow"
        
        
            return cell }
        return UICollectionViewCell()
    }
    
    
    
    // MARK: - UICollectionViewDelegate
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print("Collection did select item at: section: \(indexPath.section) item: \(indexPath.item)")
        
        performSegue(withIdentifier: segueID, sender: indexPath)
    }
    
    
    
    
    
    
    // MARK: - UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: watchListCellID, for: indexPath)

        cell.imageView?.image = UIImage(named: "Encantados")
        cell.textLabel?.text = "Encantados"
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("TableView Selction is working.")
    }
    
    

}


















