//
//  Constants.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 22/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation
import UIKit

struct PageControlAppearance {
    
    static let allPagesColor = UIColor(red: 210/255, green: 210/255, blue: 210/255, alpha: 1.0)
    static let currentPageColor = UIColor(red: 128/255, green: 128/255, blue: 128/255, alpha: 1.0)
    
    
    static func lightBackgroundStyle() {
        let appearance = UIPageControl.appearance(whenContainedInInstancesOf: [UIPageViewController.self])
        appearance.pageIndicatorTintColor = allPagesColor
        appearance.currentPageIndicatorTintColor = currentPageColor
    }
    
}



//struct Constants {
//
//    struct URL {
//        static let path = "https://www.youtube.com/embed/"
//    }
//}

