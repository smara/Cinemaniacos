//
//  MediaCell.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 26/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class MediaCell: UICollectionViewCell {
    
    @IBOutlet weak var thumbnailTrailerImage: UIImageView!
    
    @IBOutlet weak var playImage: UIImageView!
}
