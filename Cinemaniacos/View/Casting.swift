//
//  Casting.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 02/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation

struct Casting {   // por enquanto somente ator/atriz
    
    let name: String
    let job: String   // diretor, ator, atriz, roteiro
    var character: String?
    var photo: String?
    
    
    
    
    init(name: String, job: String) {
        self.name = name
        self.job = job
    }
    
    init(name: String, job: String, character: String) {
        self.init(name: name, job: job)
        self.character = character
    }
}
