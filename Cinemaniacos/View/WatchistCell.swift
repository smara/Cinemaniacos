//
//  WatchistCell.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 02/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class WatchistCell: UITableViewCell {

    @IBOutlet weak var posterImage: UIImageView!
    @IBOutlet weak var anoLabel: UILabel!
    @IBOutlet weak var duracaoLabel: UILabel!
    @IBOutlet weak var generoLabel: UILabel!
    @IBOutlet weak var classificacaoLabel: UILabel!
    @IBOutlet var rate: [UIImageView]!
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var elencoTextView: UITextView!
    
    

}
