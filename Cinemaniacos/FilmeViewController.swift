//
//  FilmeViewController.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 23/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit
import WebKit

class FilmeViewController: UIViewController, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate, WKUIDelegate {
   
    
    
    @IBOutlet weak var trailerView: UIView!
    @IBOutlet weak var posterImage: UIImageView!
    
    @IBOutlet weak var duracaoLabel: UILabel!
    @IBOutlet weak var generoLabel: UILabel!
    @IBOutlet weak var classificacaoLabel: UILabel!
    
    @IBOutlet var rate: [UIImageView]!
    
    @IBOutlet weak var tituloLabel: UILabel!
    @IBOutlet weak var diretorLabel: UILabel!
    @IBOutlet weak var trilhaSonoraLabel: UILabel!
    
    @IBOutlet weak var sinopseTextView: UITextView!
    
    @IBOutlet weak var mediaCollection: UICollectionView!
    
    let elencoCellID = "elencoCell"
    let mediaCellID = "mediaCell"
    let playImageForCV = UIImage(named: "Play")
    let trailerControllerID = "trailerVC" // Storyboard ID


    
    private(set) var trailerVC: TrailerViewController?
    var model: Movie? 
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let movie = model {
//            posterImage.image = UIImage(named: movie.posterTest)
            
            rate[0].isHighlighted = true
            rate[1].isHighlighted = true
            rate[2].isHighlighted = true
            
            duracaoLabel.text = movie.duration
            generoLabel.text = movie.genre![0]
            classificacaoLabel.text = movie.contentRating
            tituloLabel.text = movie.title
            diretorLabel.text = movie.director
//            trilhaSonoraLabel.text = movie.sound
            sinopseTextView.text = movie.synopsis
        }
        
        
    }
    
    
    
    
    
    
    // MARK: - Actions
    @IBAction func markAsWatchlistButton(_ sender: UIButton) {
    }
    @IBAction func markAsFavoriteButton(_ sender: UIButton) {
    }
    @IBAction func listenOnSpotifyButton(_ sender: UIButton) {
    }
    @IBAction func moreSynopButton(_ sender: UIButton) {
    }
    
    
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showTrailerVC" {
            trailerVC = segue.destination as? TrailerViewController
            
//            if let sender = sender as? Int {
//                if let otherMedia = model?.otherMediaURLs {
//                    trailerVC?.urlString = otherMedia[sender]
//                }
//            } else {
//                trailerVC?.urlString = model?.trailerURL
//            }
        }
    }
        
    
    
    
    // MARK: - UITableViewDataSource
    
    let images = [UIImage(named: "Jennifer"), UIImage(named: "Joel")]
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 2   // movie.elenco.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: elencoCellID, for: indexPath) as! ElencoCell
        
        if let movie = model, let elencoArray = movie.casting, elencoArray.count > indexPath.row {
            let cast = elencoArray[indexPath.row]
           
            cell.nomeLabel.text = cast.name.uppercased()
            cell.personagemLabel.text = cast.character
            cell.imageView?.image = images[indexPath.row]
//            cell.imageView.image =    Carregar a url
            
        }
//        else {    Implementar tratamento de erro para table caso não existam dados sobre o elenco
//            tableView.isHidden = true
//        }
        
        return cell
        
        
    }
    
    
    // MARK: - UICollectionViewDataSource

    // model here == model?.otherMediaURLs
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3   // model?.otherMediaURLs?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: mediaCellID, for: indexPath) as! MediaCell
        
        cell.thumbnailTrailerImage.image = UIImage(named: "OsFarofeirosImgLarge")
        cell.playImage.image = playImageForCV
        
        return cell
    }
    
    
    
    // MARK: - UICollectionViewDelegate
    var controllerTrailer: TrailerViewController?
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if let otherMedia = model?.trailers, otherMedia.count > indexPath.row {
            
//            performSegue(withIdentifier: "showTrailerVC", sender: indexPath.row)
            
            
            
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            if let controller = storyboard.instantiateViewController(withIdentifier: trailerControllerID) as? TrailerViewController {
                controllerTrailer = controller
                controller.urlString = otherMedia[indexPath.row].url
                present(controller, animated: true, completion: nil)

            }
            
        }
        
        
    }

    
    func webViewDidClose(_ webView: WKWebView) {
        controllerTrailer?.dismiss(animated: true, completion: nil)
    }

}





























