//
//  TrailerViewController.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 22/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit
import WebKit

class TrailerViewController: UIViewController {

    @IBOutlet weak var webView: WKWebView!
    
    var pageNumber: Int?
 
    var urlString: String? {
        didSet {
            if webView != nil, urlString != nil {
                getVideo(url: urlString!)
            }
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if urlString != nil {
            getVideo(url: urlString!)
        }
        
    }
    
    
    @IBAction func stopButton(_ sender: UIButton) {
        presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    
    func getVideo(url: String) {
        if let url = URL(string: urlString!) {
            webView.load(URLRequest(url: url))
        }
    }
    
}


















