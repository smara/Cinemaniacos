//
//  TrailerPageVC.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 22/03/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import UIKit

class TrailerPageVC: UIPageViewController, UIPageViewControllerDataSource {
   
    let trailerControllerID = "trailerVC"

    var trailerControllers = [TrailerViewController]()
    
    var model: [String]? 

   
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setupTrailerPageViewController()
    }
    
    private func setupTrailerPageViewController() {
        dataSource = self
        PageControlAppearance.lightBackgroundStyle()
        if let count = model?.count, count > 0 {
            if let firstController = getTrailerController(at: 0) {
                setViewControllers([firstController], direction: .forward, animated: true, completion: nil)
            }
        }
    }
    
    
    

    
    
    // MARK: - UIPageViewControllerDataSource
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        if let currentIndex = trailerControllers.index(of: (viewController as! TrailerViewController)), currentIndex > 0 {
            return getTrailerController(at: currentIndex - 1)
        }
        return nil
    }
    
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        if let currentIndex = trailerControllers.index(of: (viewController as! TrailerViewController)), let count = model?.count, currentIndex < count - 1 {
            return getTrailerController(at: currentIndex + 1)
        }
        return nil
    }
    
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return model?.count ?? 0
    }
    
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        if let currentVC = pageViewController.viewControllers?.first as? TrailerViewController {
            return currentVC.pageNumber ?? 0
        }
        return 0
    }
    
    // Helper
    private func getTrailerController(at index: Int) -> TrailerViewController? {
        guard let count = model?.count, index < count else { return nil }
        
        if index >= trailerControllers.count {
            let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
            if let controller = storyboard.instantiateViewController(withIdentifier: trailerControllerID) as? TrailerViewController {
                trailerControllers.insert(controller, at: index)
                controller.pageNumber = index
            }
        }

        trailerControllers[index].urlString = model?[index]
        return trailerControllers[index]
    }
    
    
    
    
}




















