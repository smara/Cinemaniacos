//
//  IngressoClient.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 02/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation


class IngressoClient {

    // onde ? Qual web service ?
    // o que ? path para o resource que vc quer.
    // como ? queries, parametros  e também que tipo (post, get etc)
   
        // session
        // urlrequest
            // url
                // url components (scheme, host, path, query (parameters)  )
    
        // dataTask
    
    
    // por que ?  receber respostas e usa-las
    // modelo
    
    
    func fetchMovies(completionHandler:  ([Movie]) -> Void) {
       
        
    }
    


    
    private func urlFromParameters(_ parameters: [String:AnyObject], withPathExtension: String? = nil) -> URL {
        
        var components = URLComponents()
        components.scheme = IngressoClient.Constants.ApiScheme
        components.host = IngressoClient.Constants.ApiHost
        components.path = IngressoClient.Constants.ApiPath + (withPathExtension ?? "")
        components.queryItems = [URLQueryItem]()
        
        for (key, value) in parameters {
            let queryItem = URLQueryItem(name: key, value: "\(value)")
            components.queryItems!.append(queryItem)
        }
        
        return components.url!
    }
    
}
