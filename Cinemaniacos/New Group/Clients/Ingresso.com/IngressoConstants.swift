//
//  IngressoConstants.swift
//  Cinemaniacos
//
//  Created by Silvia Florido on 02/04/18.
//  Copyright © 2018 Silvia Florido. All rights reserved.
//

import Foundation

enum Methods {
    case nowPlaying
    case nowPlayingBycity
}

extension IngressoClient {
    
    // MARK: Constants
    struct Constants {
        
        // MARK: API Key
        static let Partnership = "cinema-ja"    // API KEY
        
        // MARK: URLs
        static let ApiScheme = "https"
        static let ApiHost = "api-content.ingresso.com"
        static let ApiPath = "/v0"
        //    static let AuthorizationURL = "https://www.themoviedb.org/authenticate/"
        //    static let AccountURL = "https://www.themoviedb.org/account/"
    }
    
    // MARK: Methods
    struct IngressoApiMethods {
        
        // MARK: Events
        static let AllEvents = "/events/partnership/{partnership}"
        static let NowPlaying = "/templates/nowplaying/partnership/{partnership}"
        static let NowPlayingByCity = "/templates/nowplaying/{cityId}/partnership/{partnership}"
        static let AllByCity = "/events/city/{cityId}/partnership/{partnership}"
        
        // MARK: Location
        static let CityIDByCityName = "/states/city/name/{urlKey}"
        static let AllCitiesAndIdsByState = "/states/{uf}"
    }
    
   
    
    // MARK: URL Keys
    //struct URLKeys {
    //    static let UserID = "id"
    //}
    
    // MARK: Parameter Keys
    struct ParameterKeys {
        static let Partnership = "partnership"
        //    static let SessionID = "session_id"
        //    static let RequestToken = "request_token"
        //    static let Query = "query"
    }
    
    // For POST purposes
    //// MARK: JSON Body Keys
    //struct JSONBodyKeys {
    //    static let MediaType = "media_type"
    //    static let MediaID = "media_id"
    //    static let Favorite = "favorite"
    //    static let Watchlist = "watchlist"
    //}
    
    // MARK: JSON Response Keys
    struct JSONResponseKeys {
        
        // MARK: General
        static let Items = "items"
        static let Count = "count"
        
        // MARK: Movies
        static let MovieID = "id"
        static let MovieDuration = "duration"
        static let MovieRate = "rating"
        static let MovieContentRating = "contentRating"
        static let MovieGenres = "genres"
        static let MovieTitle = "title"
        static let MoviePremiere = "premiereDate"
        static let MovieSynopsis = "synopsis"
        static let MovieAncineID = "ancineID"
        static let MovieCast = "cast"
        static let MovieDirector = "director"
        static let MoviePriority = "priority"
        static let MovieURLKey = "urlKey"  // cidade
        static let MovieURL = "siteURL"
        static let MovieTags = "tags"
        static let MovieCountIsPlaying = "countIsPlaying"
        static let MovieTrailers = "trailers"
        static let MoviePosters = "images"
        
        
        // MARK: Trailer
        static let TrailerType = "type"
        static let TrailerURL = "url"
        static let TrailerEmbedURL = "embeddedUrl"
        
        
        // MARK: Poster
        static let PosterURL = "url"
        static let PosterType = "type"
        static let PosterHorizontal = "PosterHorizontal"
        static let PosterPortrait = "PosterPortrait"
    }
    
}


